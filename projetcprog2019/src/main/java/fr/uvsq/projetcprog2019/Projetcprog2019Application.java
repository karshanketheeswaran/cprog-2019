package fr.uvsq.projetcprog2019;


import org.springframework.boot.autoconfigure.SpringBootApplication;

import  java.lang.*;
import java.io.IOException;
import java.util.Scanner;


@SpringBootApplication
public class Projetcprog2019Application {


	public Projetcprog2019Application(){}

	public String chemindesortiejson(){
		System.out.println("Donnez le nom de votre fichier de sortie");
		String string=new String();
		Scanner scanner=new Scanner(System.in);
		string=scanner.nextLine();
		if(!(string.contains(".json")))
		{
			return chemindesortiejson();
		}
		return string;
	}
	public String chemindesortiecsv(){
		System.out.println("Donnez le nom de votre fichier de sortie");
		String string=new String();
		Scanner scanner=new Scanner(System.in);
		string=scanner.nextLine();
		if(!(string.contains(".csv")))
		{

			return chemindesortiecsv();
		}
		return string;
	}

	public void runcsv(String Filename){
		System.out.println("Avez-vous un fichier de configuration ? [Y/N]");
		String string=new String();
		Scanner scanner=new Scanner(System.in);
		string=scanner.nextLine();
		if(string.matches("[N|n]"))
		{
			string=chemindesortiejson();
			csvTojava csv = new csvTojava(Filename);
			try {
				csv.creationjson(string);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		else if(string.matches("[Y|y]"))
		{
			string=chemindesortiejson();
			System.out.println("Donnez votre fichier de configuration");
			scanner=new Scanner(System.in);
			String ConfFile=scanner.nextLine();
			csvTojava csv=new csvTojava(Filename , ConfFile);
			try {
				csv.creationviaconfjson(string);
			} catch (Exception e) {
				e.printStackTrace();
			}

		}
		else{
			runcsv(Filename);
		}
	}

	public void runjson(String Filename){
		System.out.println("Avez-vous un fichier de configuration ? [Y/N]");
		String string=new String();
		Scanner scanner=new Scanner(System.in);
		string=scanner.nextLine();
		if(string.matches("[N|n]"))
		{
			string=chemindesortiecsv();
			JsonStockage js = new JsonStockage(Filename, string);
			try {
				js.remplissageListe();
				js.toCSV();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		else if(string.matches("[Y|y]"))
		{
			string=chemindesortiecsv();
			System.out.println("Donnez votre fichier de configuration");
			scanner=new Scanner(System.in);
			String ConfFile=scanner.nextLine();
			JsonStockage js = new JsonStockage(Filename, string, ConfFile);
			try {
				js.remplissageListe();
				js.applyConfig();
			} catch (Exception e) {
				e.printStackTrace();
			}

		}
		else{
			runjson(Filename);
		}

	}
	public void run()throws IOException
	{
		System.out.println("Veuillez entrer le nom de votre fichier ? ");
		Scanner scanner=new Scanner(System.in);
		String Filename =scanner.nextLine();
		if(!(Filename.contains(".csv")|| Filename.contains(".json")))
		{
			run();
		}
		if (Filename.contains(".csv")){
			runcsv(Filename);
		}

		if(Filename.contains(".json")){
			runjson(Filename);
		}


	}
	public static void main(String[] args) throws Exception {

		Projetcprog2019Application prog=new Projetcprog2019Application();
		prog.run();

	}

}
