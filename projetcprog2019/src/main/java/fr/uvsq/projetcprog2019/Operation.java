package fr.uvsq.projetcprog2019;

public enum Operation {
    PLUS("+") {
        public double eval(double op1, double op2) {
            return op1 + op2;
        }
        public String eval(String op1, double op2) {
            return op1 + op2;
        }
        public String eval(double op1,String op2) {
            return op1 + op2;
        }
        public String eval(String op1 , String op2)
        {
            return op1+op2;
        }
    },
    MOINS("-") {
        public double eval(double op1, double op2) {
            return op1 - op2;
        }
        public String eval(String op1, double op2) {
            return "Soustraction impossible entre un String et un entier";
        }
        public String eval(double op1,String op2) {
            return "Soustraction impossible entre un entier et un String";
        }
        public String eval(String op1 , String op2)
        {
            return "Soustraction impossible entre un String et un String";
        }
    },

    MULT("*") {
        public double eval(double op1, double op2) {
            return op1 * op2;
        }
        public String eval(String op1, double op2) {
            return "Multiplication impossible entre un String et un entier";
        }
        public String eval(double op1,String op2) {
            return "Multiplication impossible entre un entier et un String";
        }
        public String eval(String op1 , String op2)
        {
            return "Multiplication impossible entre un String et un String";
        }
    },
    DIV("/") {
        public double eval(double op1, double op2) {
            if (op2==0) return 0;
            return op1 / op2;
        }
        public String eval(String op1, double op2) {
            return "Division impossible entre un String et un entier";
        }
        public String eval(double op1,String op2) {
            return "Division  impossible entre un entier et un String";
        }
        public String eval(String op1 , String op2)
        {
            return "Division impossible entre un String et un String";
        }

        };

    private String symbole;

    /**
     * Constructeur des operations qui stocke le symbole.
     *
     * @param s le symbole qui doit être stocké
     */
    Operation(String s){
        this.symbole = s;
    }

    /**
     * Getter du symbole de l'opération
     *
     * @return Le symbole
     */
    public String getSymbole(){return symbole;}

    /**
     * Fonction abstraite d'évaluation redéfinie pour chaque elément de l'opération.
     *
     * @param op1 L'opérande gauche
     * @param op2 L'opérande droite
     * @return Le resultat des deux opérandes selon l'opération
     */
    public abstract double eval(double op1, double op2);
    public abstract String eval(String op1, double op2);
    public abstract String eval(double op1, String op2);
    public abstract String eval(String op1, String op2);
}