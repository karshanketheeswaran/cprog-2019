package fr.uvsq.projetcprog2019;

import com.opencsv.*;
import  java.io.*;
import  java.util.*;
import java.lang.*;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;


public class csvTojava {

    String Filename;
    int nbelement;
    List<String> nomelem;
    List<String> ligne;
    List<String> type;
    List<List<Object>> phrase;
    List<List<Object>> listeconverti;
    int nbconverti;
    String FileConf;
    int nombredeligne;
    String Fileout;


    public csvTojava(String FileName) {
        this.Filename = FileName;
        this.nomelem = new ArrayList<>();
        this.ligne = new ArrayList<>();
        this.type = new ArrayList<>();
        this.phrase = new ArrayList<>();
        this.nbelement = 0;


    }

    public csvTojava(String FileName, String Fileconf) {
        this.Filename = FileName;
        this.FileConf = Fileconf;
        this.nomelem = new ArrayList<>();
        this.ligne = new ArrayList<>();
        this.type = new ArrayList<>();
        this.phrase = new ArrayList<>();
        this.listeconverti=new ArrayList<>();
        this.nbelement = 0;


    }


    public void remplissageliste() throws Exception {
        try {

            CSVReader readers = new CSVReader(new FileReader(this.Filename), ',');
            String[] nextLine;
            int i = 0;
            while ((nextLine = readers.readNext()) != null) {
                for (int j = 0; j < nextLine.length; j++) {
                    if (i == 0) {
                        this.nomelem.add(nextLine[j].replace(" ", ""));
                        this.nbelement = nextLine.length;
                    } else {
                        String tmp = nextLine[j].replace(" ", "");
                        ligne.add(tmp);
                    }
                }
                i++;
            }
        } catch (Exception e) {
             System.out.println("Erreur lors du remplissage de la liste"+ e);
        }

    }

    public void setphrase() {
        List<Object> list = new ArrayList<>();
        for (int i = 0; i < this.ligne.size(); i++)
        {
            if (i % this.nbelement == 0)
            {
                if (list.size() != 0)
                {
                    this.phrase.add(list);
                }
                list = new ArrayList<>();
            }
            if (this.type.get(i % this.nbelement).equals("String"))
            {
                list.add(ligne.get(i));
            }else
            {
                if (this.ligne.get(i).equals("-")) {
                    list.add(Double.parseDouble("0"));
                } else
                {
                    list.add(Double.parseDouble(ligne.get(i)));
                }
            }
        }
        this.phrase.add(list);
    }

    public void initligne() throws IOException {
        try {
            List<String> S=new ArrayList<>();
            int nbLigne = 0;
            this.ligne=new ArrayList<>();
            String lignes = null;
            File file = new File(FileConf);
            BufferedReader reader = new BufferedReader(new FileReader(file));
            try{
                // tant qu'il il a au moins une ligne à lire
                while((lignes = reader.readLine()) != null) {
                    if(lignes!=" ")
                    {
                        nbLigne++;
                        S.add(lignes);
                    }
                }
            } finally {
                reader.close();
            }
            String tmp=new String();
            for (int i = 0; i <S.size() ; i++) {

                    System.out.println(S);
                    tmp=new String();
                for (int j = S.get(i).indexOf(":")+1; j <S.get(i).length() ; j++) {
                          tmp=tmp+S.get(i).charAt(j);
                          tmp=tmp.replace("  "," ");
                }

                ligne.add(tmp);
            }
        } catch (IOException ex) {
            // erreur d'entrée/sortie ou fichier non trouvé
            ex.printStackTrace();
            throw ex;
        }
    }

    public List<List<Object>> setlisteconverti (int elem1, int elem2,Operation op,List<List<Object>> list)
    {
        List<Object> tmp=new ArrayList<>();
        if(this.type.get(elem1).equals("String")&& this.type.get(elem2).equals("String")) {

            for (int i = 0; i < this.phrase.size(); i++) {

                tmp.add(op.eval(this.phrase.get(i).get(elem1).toString(),this.phrase.get(i).get(elem2).toString()));
            }

        }
        else if(this.type.get(elem1).equals("String")&& this.type.get(elem2).equals("double")) {
            for (int i = 0; i < this.phrase.size(); i++) {
                tmp.add(op.eval(this.phrase.get(i).get(elem1).toString(),Double.valueOf(this.phrase.get(i).get(elem2).toString())));
            }
        }
        else if(this.type.get(elem1).equals("double")&& this.type.get(elem2).equals("double")) {
            for (int i = 0; i < this.phrase.size(); i++) {

                double d=op.eval(Double.valueOf(this.phrase.get(i).get(elem1).toString()),Double.valueOf(this.phrase.get(i).get(elem2).toString()));
                if(d==0.0 && op.getSymbole()=="/")
                {
                    tmp.add("Division par zéro pas possible");
                }
                else tmp.add(d);
            }
        }

        else if(this.type.get(elem2).equals("String")&& this.type.get(elem1).equals("double")) {
            for (int i = 0; i < this.phrase.size(); i++) {

                tmp.add(op.eval(Double.valueOf(this.phrase.get(i).get(elem1).toString()),this.phrase.get(i).get(elem2).toString()));
            }
        }
        this.listeconverti=list;
        this.listeconverti.add(tmp);
        return this.listeconverti;

    }
    public void remplissagetype() throws Exception {
        for (int i = 0; i < this.nbelement; i++) {

            if (ligne.get(i).equals(" ") | ligne.get(i).equals("-")) {

                int j = i + this.nbelement;
                while (j < this.ligne.size()) {
                    if (ligne.get(j).equals("-") | ligne.get(i).equals(" ")) {
                        j = j + this.nbelement;
                    } else {
                        try {

                            Double.parseDouble(ligne.get(j));
                            type.add("double");

                        } catch (NumberFormatException e) {
                            e.printStackTrace();
                            type.add("String");
                        }
                        j = this.ligne.size();
                    }
                }
            } else {
                try {

                    Double.parseDouble(ligne.get(i));
                    type.add("double");

                } catch (NumberFormatException e) {
                    type.add("String");
                }
            }
        }
    }

    public void creationjson(String FileName) throws Exception {
        this.remplissageliste();
        this.remplissagetype();
        this.setphrase();
        try (BufferedWriter buff = new BufferedWriter(new FileWriter(new File(FileName)))) {
            buff.write("[");
            buff.newLine();
            for (int i = 0; i < phrase.size(); i++) {
                buff.write("{");
                buff.newLine();
                for (int j = 0; j < this.nbelement; j++) {

                    if (phrase.get(i).get(j) instanceof String)
                        buff.write('\u0022' + this.nomelem.get(j) + '\u0022' + ":" + '\u0022' + phrase.get(i).get(j).toString() + '\u0022');
                    else buff.write('\u0022' + this.nomelem.get(j) + '\u0022' + ":" + phrase.get(i).get(j).toString());
                    if (j < this.nbelement - 1) {
                        buff.write(",");

                    }
                    buff.newLine();

                }
                if (i < this.phrase.size() - 1) buff.write("},");
                else buff.write("}");
                buff.newLine();

            }
            buff.write("]");
            buff.newLine();


        } catch (IOException e) {

        }


    }


    public void calcul() throws OutOfBoundsException
    {

        for (int i = 0; i <ligne.size() ; i++) {
            if(ligne.get(i).contains("+"))
            {
                int elem1 , elem2;
                String tmp=new String();
                for (int j = 0; j <ligne.get(i).indexOf("+"); j++)
                {
                    tmp=tmp+ligne.get(i).charAt(j);

                }
                String tmp1=new String();
                for (int j = ligne.get(i).indexOf("+")+1; j <ligne.get(i).length(); j++)
                {
                    tmp1=tmp1+ligne.get(i).charAt(j);

                }
                try{
                    elem1=Integer.parseInt(tmp);
                    elem2=Integer.parseInt(tmp1);
                    if (elem1>=nbelement || elem2>=nbelement) throw new OutOfBoundsException();
                    this.listeconverti=setlisteconverti(elem1,elem2,Operation.PLUS,this.listeconverti);


                }
                catch(NumberFormatException e){
                    e.printStackTrace();
                }
            }
            else if (ligne.get(i).contains("-"))
            {
                int elem1 , elem2;
                String tmp=new String();
                for (int j = 0; j <ligne.get(i).indexOf("-"); j++)
                {
                    tmp=tmp+ligne.get(i).charAt(j);

                }
                String tmp1=new String();
                for (int j = ligne.get(i).indexOf("-")+1; j <ligne.get(i).length(); j++)
                {
                    tmp1=tmp1+ligne.get(i).charAt(j);

                }
                try{
                    elem1=Integer.parseInt(tmp);
                    elem2=Integer.parseInt(tmp1);
                    if (elem1>=nbelement || elem2>=nbelement) throw new OutOfBoundsException();
                    this.listeconverti=setlisteconverti(elem1,elem2,Operation.MOINS,this.listeconverti);

                }
                catch(NumberFormatException e){
                    e.printStackTrace();
                }
            }
            else if (ligne.get(i).contains("*"))
            {
                int elem1 , elem2;
                String tmp=new String();
                for (int j = 0; j <ligne.get(i).indexOf("*"); j++)
                {
                    tmp=tmp+ligne.get(i).charAt(j);

                }
                String tmp1=new String();
                for (int j = ligne.get(i).indexOf("*")+1; j <ligne.get(i).length(); j++)
                {
                    tmp1=tmp1+ligne.get(i).charAt(j);

                }
                try{
                    elem1=Integer.parseInt(tmp);
                    elem2=Integer.parseInt(tmp1);
                    if (elem1>=nbelement || elem2>=nbelement) throw new OutOfBoundsException();
                    this.listeconverti=setlisteconverti(elem1,elem2,Operation.MULT,this.listeconverti);
                }
                catch(NumberFormatException e){
                    e.printStackTrace();
                }
            }
            else if(ligne.get(i).contains("/"))
            {
                int elem1 , elem2;
                String tmp=new String();
                for (int j = 0; j <ligne.get(i).indexOf("/"); j++)
                {
                    tmp=tmp+ligne.get(i).charAt(j);

                }
                String tmp1=new String();
                for (int j = ligne.get(i).indexOf("/")+1; j <ligne.get(i).length(); j++)
                {
                    tmp1=tmp1+ligne.get(i).charAt(j);

                }
                try{
                    elem1=Integer.parseInt(tmp);
                    elem2=Integer.parseInt(tmp1);
                    if (elem1>=nbelement || elem2>=nbelement) throw new OutOfBoundsException();
                    this.listeconverti=setlisteconverti(elem1,elem2,Operation.DIV,this.listeconverti);
                }
                catch(NumberFormatException e){
                    e.printStackTrace();
                }
            }
        }


    }

    public void creationviaconfjson(String output) throws Exception {
        this.remplissageliste();
        this.remplissagetype();
        this.setphrase();
        this.initligne();
        this.calcul();
        try (BufferedWriter buff = new BufferedWriter(new FileWriter(new File(output)))) {
            buff.write("[");
            buff.newLine();
            int k=0;
            for (int i = 0; i < this.listeconverti.get(k).size(); i++) {
                buff.write("{");
                buff.newLine();
                for (int j = 0; j < listeconverti.size(); j++) {
                    String val=""+j;
                    if (listeconverti.get(j).get(i) instanceof String)
                        buff.write('\u0022' +val+ '\u0022' + ":" + '\u0022' + listeconverti.get(j).get(i).toString() + '\u0022');
                    else buff.write('\u0022' +val+ '\u0022' + ":" + listeconverti.get(j).get(i).toString());
                    if (j < this.listeconverti.size() - 1) {
                        buff.write(",");

                    }
                    buff.newLine();

                }
                if (k < this.listeconverti.get(k).size() - 1) {
                    buff.write("},");
                    k=k+1;
                }
                else buff.write("}");
                buff.newLine();

            }
            buff.write("]");
            buff.newLine();


        } catch (IOException e) {

        }



    }
}
