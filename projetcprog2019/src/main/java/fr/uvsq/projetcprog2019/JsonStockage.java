package fr.uvsq.projetcprog2019;


import com.fasterxml.jackson.core.*;
import com.fasterxml.jackson.databind.*;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.File;
import java.io.IOException;
import java.util.*;



public class JsonStockage {

    private List<String> noms;
    private List<String> valeurs;
    private String cheminEntree;
    private String cheminSortie;
    private String fichier;
    private String config;

    public JsonStockage(String entree, String sortie) {

        this.noms = new ArrayList<String>();
        this.valeurs = new ArrayList<String>();
        this.cheminEntree = entree;
        this.cheminSortie = sortie;

    }

    public JsonStockage(String entree, String sortie, String config) {

        this.noms = new ArrayList<String>();
        this.valeurs = new ArrayList<String>();
        this.cheminEntree = entree;
        this.cheminSortie = sortie;
        this.config = config;
    }

    public String getCheminEntree() {
        return cheminEntree;
    }

    public String getCheminSortie() {
        return cheminSortie;
    }

    public List<String> getNoms() {
        return noms;
    }

    public List<String> getValeurs() {
        return valeurs;
    }

    public String getFichier() {
        return fichier;
    }

    public String getConfig() {
        return config;
    }

    public void setNoms(List<String> noms) {
        this.noms = noms;
    }

    public void setValeurs(List<String> valeurs) {
        this.valeurs = valeurs;
    }

    public void setCheminEntree(String cheminEntree) {
        this.cheminEntree = cheminEntree;
    }

    public void setCheminSortie(String cheminSortie) {
        this.cheminSortie = cheminSortie;
    }

    public void setFichier() {
        try (BufferedReader bufferedreader = new BufferedReader(new FileReader(this.cheminEntree))) {
            String strCurrentLine;
            while ((strCurrentLine = bufferedreader.readLine()) != null) {
                fichier+=strCurrentLine;
            }
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }

    public void setConfig(String config) {
        this.config = config;
    }

    public void remplissageListe() {
        JsonFactory factory = new JsonFactory();
        try {
            setFichier();
            JsonParser parser = factory.createParser(this.fichier);
            String profondeur = "";
            String passage = "";
            boolean dejavu = false;
            while (!parser.isClosed()) {
                JsonToken jsonToken = parser.nextToken();
                if (jsonToken == JsonToken.FIELD_NAME) {
                    passage = parser.getText();

                }
                else if (jsonToken == JsonToken.VALUE_STRING || jsonToken == JsonToken.VALUE_NUMBER_INT || jsonToken == JsonToken.VALUE_NUMBER_FLOAT){
                    if (!this.noms.contains(profondeur + "." + passage) && !this.noms.contains(profondeur)) {
                        this.noms.add (profondeur + "." + passage);
                    }
                    else {
                        if (this.noms.contains(profondeur + "." + passage) && dejavu == false){
                            int indice = this.noms.indexOf(profondeur + "." + passage);
                            for (int i = 0; i < indice ; i++) {
                                this.valeurs.add(valeurs.get(i));
                            }
                            dejavu=true;
                        }
                        else if (this.noms.contains(profondeur) && dejavu == false){
                            int indice = this.noms.indexOf(profondeur);
                            for (int i = 0; i < indice ; i++) {
                                this.valeurs.add(valeurs.get(i));
                            }
                            dejavu=true;
                        }
                    }
                    valeurs.add(parser.getText());


                }
                else if (jsonToken == JsonToken.START_OBJECT){

                    if (profondeur != "" && passage != "")profondeur= profondeur + "." + passage;
                    else if (profondeur == "" && passage != "") profondeur=passage;

                }
                else if (jsonToken == JsonToken.END_OBJECT){
                    passage = "";
                    dejavu=false;
                }

            }
        }
        catch(IOException e){
            e.printStackTrace();
        }

    }
    public String typeval (int elem) {
        if (valeurs.get(elem).equals(" ") | valeurs.get(elem).equals("-")) {

            int j = elem + noms.size();
            while (j < this.valeurs.size()) {
                if (valeurs.get(j).equals("-") | valeurs.get(j).equals(" ")) {
                    j = j + noms.size();
                } else {
                    try {

                        Double.parseDouble(valeurs.get(j));
                        return "double";

                    } catch (NumberFormatException e) {
                        return "String";
                    }
                }
            }
        } else {
            try {

                Double.parseDouble(this.valeurs.get(elem));
                return "double";
            } catch (NumberFormatException e) {

            }
        }
        return "String";
    }
    public List<String> setlisteconverti (int elem1, int elem2,Operation op,List<String> list)
    {
        //i = elem1, i<taille tableau valeur; i = i+taille tableau nom
        String type1=typeval(elem1);
        String type2=typeval(elem2);
        if(type1.equals("String")&& type2.equals("String")) {

            for (int i = elem1; i < this.valeurs.size(); i = i + this.noms.size()) {
                list.add(op.eval(valeurs.get(i),this.valeurs.get(elem2)));
                elem2=elem2+this.noms.size();

                }
        }

        else if(type1.equals("String")&& type2.equals("double")) {
            for (int i = elem1; i < this.valeurs.size(); i = i + this.noms.size()) {
                list.add(op.eval(valeurs.get(i),Double.valueOf(valeurs.get(elem2))));
                elem2=elem2+this.noms.size();
            }
        }
        else if(type1.equals("double")&& type2.equals("String")) {
            for (int i = elem1; i < this.valeurs.size(); i = i + this.noms.size()) {
                list.add(op.eval(Double.valueOf(valeurs.get(i)),valeurs.get(elem2)));
                elem2=elem2+this.noms.size();
            }
        }
        else if(type2.equals("double")&& type1.equals("double")) {
            for (int i = elem1; i < this.valeurs.size(); i = i + this.noms.size()) {
                double d=op.eval(Double.valueOf(this.valeurs.get(i)),Double.valueOf(this.valeurs.get(elem2)));

                if(d==0.0 && op.getSymbole()=="/")
                {
                    list.add("Division par zéro pas possible");
                }

                else list.add(d+"");
                elem2=elem2+this.noms.size();
            }
        }
        return list;

    }

    public List<String> calcul(List<String> op)throws OutOfBoundsException{
        List<String> result = new ArrayList<>();
        for (int i = 0; i <op.size() ; i++) {
            if(op.get(i).contains("+"))
            {
                int elem1 , elem2;
                String tmp=new String();
                for (int j = 0; j <op.get(i).indexOf("+"); j++)
                {
                    tmp=tmp+op.get(i).charAt(j);

                }
                String tmp1=new String();
                for (int j = op.get(i).indexOf("+")+1; j <op.get(i).length(); j++)
                {
                    tmp1=tmp1+op.get(i).charAt(j);

                }
                try{
                    elem1=Integer.parseInt(tmp);
                    elem2=Integer.parseInt(tmp1);
                    if (elem1>=noms.size() || elem2>=noms.size()) throw new OutOfBoundsException();
                    result=setlisteconverti(elem1,elem2,Operation.PLUS,result);

                }
                catch(NumberFormatException e){
                    e.printStackTrace();
                }
            }
            else if (op.get(i).contains("-"))
            {
                int elem1 , elem2;
                String tmp=new String();
                for (int j = 0; j <op.get(i).indexOf("-"); j++)
                {
                    tmp=tmp+op.get(i).charAt(j);

                }
                String tmp1=new String();
                for (int j = op.get(i).indexOf("-")+1; j <op.get(i).length(); j++)
                {
                    tmp1=tmp1+op.get(i).charAt(j);

                }
                try{
                    elem1=Integer.parseInt(tmp);
                    elem2=Integer.parseInt(tmp1);

                    if (elem1>=noms.size() || elem2>=noms.size()) throw new OutOfBoundsException();
                    result=setlisteconverti(elem1,elem2,Operation.MOINS,result);

                }
                catch(NumberFormatException e){
                    e.printStackTrace();
                }
            }
            else if (op.get(i).contains("*"))
            {
                int elem1 , elem2;
                String tmp=new String();
                for (int j = 0; j <op.get(i).indexOf("*"); j++)
                {
                    tmp=tmp+op.get(i).charAt(j);

                }
                String tmp1=new String();
                for (int j = op.get(i).indexOf("*")+1; j <op.get(i).length(); j++)
                {
                    tmp1=tmp1+op.get(i).charAt(j);

                }
                try{
                    elem1=Integer.parseInt(tmp);
                    elem2=Integer.parseInt(tmp1);
                    if (elem1>=noms.size() || elem2>=noms.size()) throw new OutOfBoundsException();
                    result=setlisteconverti(elem1,elem2,Operation.MULT,result);

                }
                catch(NumberFormatException e){
                    e.printStackTrace();
                }
            }
            else if(op.get(i).contains("/"))
            {
                int elem1 , elem2;
                String tmp=new String();
                for (int j = 0; j <op.get(i).indexOf("/"); j++)
                {
                    tmp=tmp+op.get(i).charAt(j);

                }
                String tmp1=new String();
                for (int j = op.get(i).indexOf("/")+1; j <op.get(i).length(); j++)
                {
                    tmp1=tmp1+op.get(i).charAt(j);

                }
                try{
                    elem1=Integer.parseInt(tmp);
                    elem2=Integer.parseInt(tmp1);
                    if (elem1>=noms.size() || elem2>=noms.size()) throw new OutOfBoundsException();
                    result=setlisteconverti(elem1,elem2,Operation.DIV,result);

                }
                catch(NumberFormatException e){
                    e.printStackTrace();
                }
            }
        }

        return result;
    }
    public void toCSVconfig(List<String>list ,List<String> name)
    {
        try(BufferedWriter buf = new BufferedWriter(new FileWriter(new File(cheminSortie)))){
            int i = 1;
            for (String s : name){
                if (i==name.size()){
                    buf.write(s+"\n");
                    i=1;
                }
                else{
                    buf.write(s+",");
                    i++;
                }
            }
            i=1;
            int k=0;
            for (int j = 0; j < this.valeurs.size()/this.noms.size(); j++) {
                k=j;
                while(k<list.size()) {
                    if (i==name.size()){
                        buf.write(list.get(k)+"\n");
                        i=1;
                    }
                    else{
                        buf.write(list.get(k)+",");
                        i++;
                    }
                    k=k+this.valeurs.size()/this.noms.size();
                }
            }
        }
        catch(IOException e){
            e.printStackTrace();
        }
    }


    public void applyConfig() throws IOException, OutOfBoundsException {
        List<String> nomsT = new ArrayList<String>();
        List<String> valeursT = new ArrayList<String>();
        List<String> operations = new ArrayList<String>();
        List<String> S=new ArrayList<>();
        String lignes = null;
        BufferedReader reader = new BufferedReader(new FileReader(new File(config)));
        try{
            // tant qu'il il a au moins une ligne à lire
            while((lignes = reader.readLine()) != null) {
                if(lignes!=" ")
                {
                    S.add(lignes);
                }
            }
        } finally {
            reader.close();
        }
        String tmp=new String();
        for (int i = 0; i <S.size() ; i++) {
            tmp=new String();
            for (int j =0; j<S.get(i).indexOf(":") ; j++) {
                tmp=tmp+S.get(i).charAt(j);
            }

            nomsT.add(tmp);
        }
        for (int i = 0; i <S.size() ; i++) {
            tmp = new String();
            for (int j = S.get(i).indexOf(":") + 1; j < S.get(i).length(); j++) {
                tmp = tmp + S.get(i).charAt(j);
                tmp = tmp.replace("  ", " ");
            }

            operations.add(tmp);
        }
        valeursT=calcul(operations);
        toCSVconfig(valeursT, nomsT);
    }




    public void toCSV(){
        try(BufferedWriter buf = new BufferedWriter(new FileWriter(new File(cheminSortie)))){
            int i = 1;
            for (String s : noms){
                if (i==noms.size()){
                    buf.write(s+"\n");
                    i=1;
                }
                else{
                    buf.write(s+",");
                    i++;
                }
            }
            for (String s : valeurs){

                if (i==noms.size()){
                    buf.write(s+"\n");
                    i=1;
                }
                else{
                    buf.write(s+",");
                    i++;
                }
            }
        }
        catch(IOException e){
            e.printStackTrace();
        }
    }

}

