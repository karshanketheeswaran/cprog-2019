package fr.uvsq.projetcprog2019;

public class OutOfBoundsException extends Exception {

  public OutOfBoundsException() {
    super("la valeur ne peut être traité.");
  }

}