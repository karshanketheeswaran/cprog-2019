package fr.uvsq.projetcprog2019;


import org.junit.Test;

import static org.junit.Assert.*;

public class OperationTest {
  
  
  @Test
  public void ExistanceEnum() {
    assertNotNull(Operation.valueOf("PLUS"));
    assertNotNull(Operation.valueOf("MOINS"));
    assertNotNull(Operation.valueOf("DIV"));
    assertNotNull(Operation.valueOf("MULT"));
  }
  
  @Test
  public void getSymbole() {
    Operation plus = Operation.PLUS;
    assertEquals("+", plus.getSymbole());
    
    Operation moins = Operation.MOINS;
    assertEquals("-", moins.getSymbole());
    
    Operation mult = Operation.MULT;
    assertEquals("*", mult.getSymbole());
    
    Operation div = Operation.DIV;
    assertEquals("/", div.getSymbole());
    
  }
  
  @Test
  public void eval() {
    Operation opPlus = Operation.PLUS;
    assertEquals(3.0,opPlus.eval(1.0,2.0),0);
    assertEquals(1.0,opPlus.eval(-1.0,2.0),0);
    assertEquals("T2.0",opPlus.eval("T",2));
    assertEquals("1.0T",opPlus.eval(1.0,"T"));
  }
  
  @Test
  public void testEval() {
    Operation opMoins = Operation.MOINS;
    assertEquals(1.0,opMoins.eval(2.0,1),0);
    assertEquals("Soustraction impossible entre un String et un entier",
      "Soustraction impossible entre un String et un entier",opMoins.eval("f",2.0));
    assertEquals("Soustraction impossible entre un String et un entier",
      "Soustraction impossible entre un entier et un String",opMoins.eval(2.0,"f"));
    assertEquals("imposssible","Soustraction impossible entre un String et un String"
      ,opMoins.eval("s","s"));
  }
  
  @Test
  public void testEval1() {
    Operation opDiv = Operation.DIV;
    assertEquals(0,opDiv.eval(1.0,0),0);
    assertEquals(1.0,opDiv.eval(2.0,2),0);
    assertEquals("impossible","Division impossible entre un String et un entier",
      opDiv.eval("S",2.0));
    assertEquals("impossible","Division  impossible entre un entier et un String",
      opDiv.eval(2.0,"S"));
    assertEquals("impossible","Division impossible entre un String et un String",
      opDiv.eval("S","S"));
  }
  
  @Test
  public void testEval2() {
    Operation opMult = Operation.MULT;
    assertEquals(4.0,opMult.eval(2.0,2),0);
    assertEquals("impossible","Multiplication impossible entre un String et un entier",
      opMult.eval("s",2.0));
    assertEquals("impossible","Multiplication impossible entre un entier et un String",
      opMult.eval(2.0,"f"));
    assertEquals("impossible","Multiplication impossible entre un String et un String",
      opMult.eval("s","s"));
  }
}