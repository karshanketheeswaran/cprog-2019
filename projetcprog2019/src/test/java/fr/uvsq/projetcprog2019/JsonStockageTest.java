package fr.uvsq.projetcprog2019;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.fasterxml.jackson.core.*;
import com.fasterxml.jackson.databind.*;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.File;
import java.io.IOException;
import java.util.*;

import static org.junit.Assert.*;

public class JsonStockageTest{

    @Test
    public void TestConstructeur2(){
        JsonStockage js = new JsonStockage("fichier.json", "fichier_s.csv");
        assertEquals(js.getCheminEntree(),"fichier.json");
        assertEquals(js.getCheminSortie(),"fichier_s.csv");
    }

    @Test
    public void TestConstructeur3(){
        JsonStockage js = new JsonStockage("fichier.json", "fichier_s.csv", "conf.txt");
        assertEquals(js.getCheminEntree(),"fichier.json");
        assertEquals(js.getCheminSortie(),"fichier_s.csv");
        assertEquals(js.getConfig(),"conf.txt");
    }

   @Test (expected = NullPointerException.class)
    public void remplissageListeAvecFichierEntreeVide(){
       JsonStockage js = new JsonStockage("", "fichier_s.csv");
       js.remplissageListe();
   }



}