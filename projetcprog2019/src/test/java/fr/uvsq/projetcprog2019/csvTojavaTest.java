package fr.uvsq.projetcprog2019;

import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import static org.junit.Assert.assertEquals;

public class csvTojavaTest {
    
    @Test
    public void remplissageliste() throws Exception {
        csvTojava csvTojava = new csvTojava("test.csv");
        csvTojava.remplissageliste();
        List<String> list = new ArrayList<>();
        list.add("album");
        list.add("year");
        list.add("US_peak_chart_post");
        assertEquals(list,csvTojava.nomelem);
    }
    
    @Test
    public void initligne() throws IOException {
        csvTojava csvTojava = new csvTojava("test.csv","conf.txt");
        csvTojava.initligne();
        assertEquals(4,csvTojava.ligne.size());
    }
    
    @Test(expected = IOException.class)
    public void initligneException() throws IOException {
        csvTojava csvTojava = new csvTojava("tt.csv","sf.txt");
        csvTojava.initligne();
    }
    
    @Test
    public void remplissagetype() throws Exception {
        csvTojava csvTojava = new csvTojava("test.csv","src/main/resources/conf.txt");
        csvTojava.remplissageliste();
        csvTojava.remplissagetype();
        List<String> list = new ArrayList<>();
        list.add("String");
        list.add("double");
        list.add("double");
        
        assertEquals(list,csvTojava.type);
    }
    
    @Test
    public void creationjson() throws Exception {
        csvTojava csvTojava = new csvTojava("test.csv","src/main/resources/conf.txt");
        csvTojava.creationjson("Tester.txt");
        File file =  new File("Tester.txt");
        Scanner sc = new Scanner(file);
        assertEquals("[",sc.nextLine());
        assertEquals("{",sc.nextLine());
    }
    
    
}