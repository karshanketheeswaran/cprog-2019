# PROJET CPROG : CONVERTISSEUR JSON CSV

Ce projet est un convertisseur de fichiers JSON vers CSV et CSV vers JSON à l'aide d'un fichier de conf 


## Démarrage et utilisation

Pour lancer le convertisseur, exécuter l'une des commandes suivantes : 

_ ./mvnw spring-boot:run 

_ java -jar target/projetcprog2019-0.0.1-SNAPSHOT.jar

Une fois lancé, le programme vous demande le nom du fichier (*.json ou *.csv)  que vous voulez convertir. 
Puis vous pouvez donner un fichier de configuration contenant les opérations que vous voulez effectuer sur les 
différents champs de votre fichier initial.
Pour vous aider un fichier de configuration est fourni avec les sources, il se situe "conf.txt"
Vous pouvez aussi exécuter le programme sans fichier de configuration. Si vous donnez un fichier .json vous aurez 
un fichier .csv en sortie et vice versa. 

## Fabriqué avec 

Le convertisseur à été développé en utilisant :

* maven wrapper

* JUnit - v. 4.13

* com.opencsv - v. 3.7

* com.fasterxml.jackson.dataformat - v. 2.9.8

* com.fasterxml.jackson.core - v. 2.10.1

* IntelliJ

## Auteurs 
La liste des auteurs de ce convertisseur :

* **Elarif Hamidou**

* **Karshan Ketheeswaran**

* **Patrick Trestka**